import axios from "axios";
import { useEffect, useState } from "react";
import BasketDrawer from "./components/basketDrawer/BasketDrawer";
import Header from "./components/header/Header";
import SneakersCard from "./components/sneakersCard/SneakersCard";

function App() {
  const [items, setItems] = useState([]);
  const [basketItems, setBasketItems] = useState([]); // массив для хранения товаров в корзине
  const [favorites, setFavorites] = useState([]);
  const [searchValue, setSearchValue] = useState(""); // стейт для хранения данных, которые пользователь вписывает в поисковую строку

  const [basketOpened, setBasketOpened] = useState(false); // открыта ли корзина

  useEffect(() => {
    axios
      .get("https://62a8ebacec36bf40bdb0718a.mockapi.io/Items")
      .then((res) => {
        setItems(res.data);
      });

    axios
      .get("https://62a8ebacec36bf40bdb0718a.mockapi.io/cart")
      .then((res) => {
        setBasketItems(res.data);
      });
  }, []);

  const onAddToBasket = (item) => {
    axios.post("https://62a8ebacec36bf40bdb0718a.mockapi.io/cart", item);

    // basketItems.includes(item)
    //   ? deleteFromBasket(item)
    //   : // если передать в useState (в set) функцию, то она берет предыдущие данные из basketItems (или которые есть сейчас)
    setBasketItems((previous) => [...previous, item]); // то же самое, что basketItems.push(item) - но в реакте нужно добавлять элемент в конец самостотельно
    // создается новый массив, в который помещаются предыдущие данные и новое значение
  };

  const deleteFromBasket = (item) => {
    axios.delete(`https://62a8ebacec36bf40bdb0718a.mockapi.io/cart/${item.id}`);
    setBasketItems(basketItems.filter((prev) => prev.id !== item.id));
    // фильтруем предыдущий массив по условию
  };

  const onChangeSearchInput = (event) => {
    // console.log(event.target.value); // вывод всего, что вводит пользователь
    setSearchValue(event.target.value);
  };

  const onAddToFavorite = (item) => {
    axios.post("https://62a8ebacec36bf40bdb0718a.mockapi.io/favorites", item);

    setFavorites((previous) => [...previous, item]); // обновляем массив
  };

  return (
    <div className="wrapper clear">
      {basketOpened && (
        <BasketDrawer
          items={basketItems}
          onClose={() => setBasketOpened(false)}
          onDelete={deleteFromBasket}
        />
      )}
      {/* если первое значение false, выполнится второе */}
      {/* {basketOpened ? (
        <BasketDrawer onClose={() => setBasketOpened(false)} />
      ) : null} */}
      <Header onClickBasket={() => setBasketOpened(true)} />
      <div className="content p-40">
        {/* тут надпись 'все кроссовки' и поиск */}
        <div className="d-flex align-center justify-between">
          <h1>
            {searchValue ? `Поиск по запросу: "${searchValue}"` : `All shoes`}
          </h1>
          <div className="search-block d-flex">
            <img width={15} height={15} src="/img/search.png" alt="Search" />
            {searchValue && (
              <img
                onClick={() => setSearchValue("")}
                className="clear cu-p" // cursor-pointer (macro-css)
                width={30}
                height={30}
                src="/img/cancel.svg"
                alt="Clear"
              />
            )}
            <input
              onChange={onChangeSearchInput}
              value={searchValue}
              placeholder="Search..."
            />
          </div>
        </div>
        <div className="sneakers">
          {items
            .filter((item) =>
              item.title.toLowerCase().includes(searchValue.toLowerCase())
            )
            .map((item, key) => {
              return (
                <SneakersCard
                  title={item.title}
                  price={item.price}
                  sneakersImage={item.sneakersImage}
                  clickOnFavorite={() => onAddToFavorite(item)}
                  clickOnPlus={() => onAddToBasket(item)}
                  basketItems={item}
                  items={items}
                  key={key}
                />
              );
            })}
        </div>
      </div>
    </div>
  );
}

export default App;
