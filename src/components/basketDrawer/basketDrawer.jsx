import React from "react";

const BasketDrawer = ({ items = [], onClose, onDelete }) => {
  const onDeleteClick = (obj) => {
    onDelete(obj);
  };

  return (
    <div className="mainShadow">
      <div className="drawer">
        <h2 className="d-flex justify-between mb-30">
          Basket{" "}
          <img
            className="basketRemove cu-p" // cursor-pointer (macro-css)
            width={30}
            height={30}
            src="/img/cancel.svg"
            alt="Close"
            onClick={onClose}
          />
        </h2>
        {items.length > 0 ? (
          <div>
            <div className="items">
              {items.map((obj, key) => (
                <div className="basketItem d-flex align-center mb-20" key={key}>
                  <div
                    style={{ backgroundImage: `url(${obj.sneakersImage})` }}
                    className="basketItemImg"
                  ></div>
                  <div className="flex">
                    <p className="mb-5">{obj.title}</p>
                    <b>{obj.price} rub.</b>
                  </div>
                  <img
                    className="basketRemove"
                    width={30}
                    height={30}
                    src="/img/cancel.svg"
                    alt="Remove"
                    onClick={() => onDeleteClick(obj)}
                  />
                </div>
              ))}
            </div>
            <div className="basketTotalBlock">
              <ul>
                <li>
                  <span>Total:</span>
                  <div></div>
                  <b>21 498 rub.</b>
                </li>
                <li>
                  <span>Tax 5%:</span>
                  <div></div>
                  <b>1074 rub.</b>
                </li>
              </ul>
              <button className="greenButton">
                Order <img src="/img/arrow.svg" alt="Arrow" />
              </button>
            </div>
          </div>
        ) : (
          <div class="basketEmpty d-flex align-center justify-center flex-column flex">
            <img
              class="mb-20"
              width="120px"
              height="120px"
              src="/img/emptyBox.svg"
              alt="Empty Cart"
            />
            <h2>Basket is empty</h2>
            <p class="opacity-6">Add sneakers to order</p>
            <button onClick={onClose} className="greenButton">
              <img src="/img/arrow.svg" alt="Arrow" />
              Back to home
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default BasketDrawer;
